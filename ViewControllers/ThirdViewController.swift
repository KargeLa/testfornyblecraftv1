//
//  ThirdViewController.swift
//  testForNyblecraft
//
//  Created by Алексей Смоляк on 8/18/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {
    
    //    MARK: - Properties
    var info : RealmWeather!
    
    //    MARK: - Outlets
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var region: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var lat: UILabel!
    @IBOutlet weak var lon: UILabel!
    @IBOutlet weak var localTime: UILabel!
    @IBOutlet weak var temp_c: UILabel!
    @IBOutlet weak var lastUpdated: UILabel!
    
    //    MARK: - Actions
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - LifeCyrcle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = "City:" + " " + "\(info.location!.name!)"
        region.text = "Region:" + " " +  "\(info.location!.region!)"
        country.text = "Country:" + " " + "\(info.location!.country!)"
        lat.text = "Lat:" + " " + "\(info.location!.lat)"
        lon.text = "Lon:" + " " + "\(info.location!.lon)"
        localTime.text = "LocalTime:" + " " + "\(info.location!.localtime!)"
        temp_c.text = "Temp_C:" + " " + "\(info.current!.temp_c)"
        lastUpdated.text = "Last_Update:" + " " + "\(info.current!.last_updated!)"
    }
    
}
