import UIKit
import MapKit
import CoreLocation
import RealmSwift

class ViewController: UIViewController {
    
    //    MARK: - Properties
    var locationManager:CLLocationManager!
    var weatherArray : [RealmWeather] = []
    var isRequestingWeather: Bool = false
    //    MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    
    //    MARK: - LifeCyrcle
    deinit {
        print("deinit viewcontroller1")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(Realm.Configuration.defaultConfiguration.fileURL)")
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func transliterate(nonLatin: String) -> String {
        return nonLatin
            .applyingTransform(.toLatin, reverse: false)?
            .applyingTransform(.stripDiacritics, reverse: false)?
            .lowercased()
            .replacingOccurrences(of: " ", with: "-") ?? nonLatin
    }
    
}

extension ViewController : CLLocationManagerDelegate {
    
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("update locations")
        let userLocation :CLLocation = locations[0] as CLLocation
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        mapView.mapType = MKMapType.standard
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        self.latLabel.text = "Lat:\(userLocation.coordinate.latitude)"
        self.lonLabel.text = "Lon:\(userLocation.coordinate.longitude)"
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            guard let placemarks = placemarks else { return }
            if (error != nil) {
                print("error in reverseGeocode")
            }
            
            if placemarks.count > 0 {
                let placemark = placemarks[0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)
                
                self.locationLabel.text = "\(placemark.locality!)"
                
                if self.isRequestingWeather == false {
                    self.isRequestingWeather = true
                    ApiManager.instance.getWether(parameters: ["city":placemark.locality!] ) { (wetherFromServer/* название задаю сам */) in
                        self.weatherArray = wetherFromServer
                        print(self.weatherArray.count)
                        let infAboutWether = self.weatherArray[0]
                        
                        self.temperatureLabel!.text = "Сейчас " + "\(infAboutWether.current!.temp_c)" + "°"
                        DataBaseManager.instance.saveWetherToDataBase(wether: self.weatherArray)
                        self.isRequestingWeather = false
                    }
                }
            }
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
}


