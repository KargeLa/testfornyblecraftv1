import UIKit

class SecondViewController: UIViewController {
    
    //    MARK: - Properties
    var infoArray : [RealmWeather] = []
    
    //    MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - LifeCyrcle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        infoArray = DataBaseManager.instance.getObjects(RealmWeather.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
}

extension SecondViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SecondVCTableViewCell
        
        let info = infoArray[indexPath.row]
        cell.timeLabel.text = "Localtime" + " " + "\(info.location!.localtime!)"
        cell.cityLabel.text = "City:" + " " + "\(info.location!.name!)"
        cell.latLabel.text = "Lat:" + " " + "\(info.location!.lat)"
        cell.lonLabel.text = "Lon:" + " " + "\(info.location!.lon)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stroryBoard = UIStoryboard(name: "Main", bundle: nil)
        let thirdVC = stroryBoard.instantiateViewController(withIdentifier: "ThirdViewController") as! ThirdViewController
        let info = infoArray[indexPath.row]
        thirdVC.info = info
        navigationController?.pushViewController(thirdVC, animated: true)
    }
}
