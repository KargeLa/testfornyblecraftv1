import  RealmSwift
import Foundation
class RealmLocation: Object {
    
    @objc dynamic var name: String?
    @objc dynamic var region: String?
    @objc dynamic var country: String?
    @objc dynamic var lat: Double = 0
    @objc dynamic var lon: Double = 0
    @objc dynamic var localtime: String?
    @objc dynamic var id: String = UUID().uuidString
    @objc var desc: String {
        return """
        name: \(name)
        region: \(region)
        country: \(country)
        lat: \(lat)
        lon: \(lon)
        localtime: \(localtime)
        """
    }
    override static func primaryKey() -> String? {
        return "id"
    }
}
