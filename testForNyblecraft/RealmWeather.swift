import  RealmSwift
import Foundation
class RealmWeather: Object {
    @objc dynamic var location: RealmLocation?
    @objc dynamic var current: RealmCurrent?
    @objc dynamic var id: String = UUID().uuidString
    @objc var desc: String {
        return """
        location: \(location)
        current: \(current)
        """
    }
    override static func primaryKey() -> String? {
        return "id"
    }
}
