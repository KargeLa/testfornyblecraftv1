
import UIKit
import MapKit

class WetherCellTableViewCell: UITableViewCell {

    //    MARK: - Outlets
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var lat: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var log: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
