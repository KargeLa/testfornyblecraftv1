import Foundation
import RealmSwift



class DataBaseManager {
    static var instance = DataBaseManager()
    func save<T>(_ object: [T]) {
        let realm = try! Realm()
        try! realm.write {
            if let objects = object as? [Object] {
                realm.add(objects, update: true)
            }
        }
    }
    
    func save<T: Object>(_ object: [T]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    func getObjects<T: Object>(_ classType: T.Type) -> [T] {
        let realm = try! Realm()
        let result = realm.objects(T.self)
        return Array(result)
    }
    
    
    func saveWetherToDataBase (wether: [RealmWeather]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(wether, update: true)
        }
    }
    
    func getWetherFromDataBase() -> [RealmWeather] {
        let realm = try! Realm()
        let realmWetherResult = realm.objects(RealmWeather.self)
        //        usersResult[0]
        var wether: [RealmWeather] = []
        for realmWether in realmWetherResult {
            wether.append(realmWether)
        }
        return wether
    }
}
