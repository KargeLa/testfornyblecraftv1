import Foundation
import Alamofire


class ApiManager {
    static var instance = ApiManager()
    private enum Constants {
        static let baseURL = "http://api.apixu.com/v1/current.json?key=ecd8ad1593824467820201542192008&q="
    }
//    http://api.apixu.com/v1/current.json?key=74cb846a002b40cda0c122907191508%20&q=
    
    func getWether(parameters: [String: Any], onComplete: @escaping ([RealmWeather]) -> Void) {
        let cityName = parameters["city"]
        
        let latinString = (cityName! as AnyObject).applyingTransform(StringTransform.toLatin, reverse: false)
        let noDiacriticString = latinString?.applyingTransform(StringTransform.stripDiacritics, reverse: false)
        
        let urlString = Constants.baseURL + "\(latinString!)"
        AF.request(urlString, method: .get, parameters: [:]) .responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                print(data)
                if let wethersDict = data as? Dictionary<String, Any> {
                    var wethers = RealmWeather()
                    if let locationDict = wethersDict["location"] as? Dictionary<String, Any> {
                        let location = RealmLocation()
                        location.name = locationDict["name"] as? String ?? ""
                        location.region = locationDict["region"] as? String ?? ""
                        location.country = locationDict["country"] as? String ?? ""
                        location.localtime = locationDict["localtime"] as? String ?? ""
                        location.lat = locationDict["lat"] as? Double ?? 0
                        location.localtime = locationDict["localtime"] as? String ?? ""
                        location.lon = locationDict["lon"] as? Double ?? 0
                        
                        wethers.location = location
                    }
                    if let currentDict = wethersDict["current"] as? Dictionary<String, Any> {
                        var current = RealmCurrent()
                        current.temp_c = currentDict["temp_c"] as? Double ?? 0
                        current.last_updated = currentDict["last_updated"] as? String ?? ""
                        
                        wethers.current = current
                    }
                    onComplete([wethers])
                    
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
    
    
    
}

